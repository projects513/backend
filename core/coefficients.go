package core

import "time"

const (
	WORKER_BUS_DRIVER_EXTRA_SMALL = iota + 1
	WORKER_BUS_DRIVER_SMALL
	WORKER_BUS_DRIVER_STANDART
	WORKER_BUS_DRIVER_BIG
	WORKER_BUS_DRIVER_EXTRA_BIG
	WORKER_BUS_CONDUCTOR
	WORKER_BUS_REPAIRMAN
)

type Coefficients struct {
	// Среднемесячная номинальная начисленная заработная плата работников
	// крупных и средних предприятий и некоммерческих организаций всех отраслей
	// экономики за ближайший истекший отчетный период
	// Источник: Росстат
	// Обозначение: СЗП
	AvgSalary float32

	// Суммарный тариф отчислений на социальные нужды и обязательное социальное страхование
	// Источник: Законодательство РФ
	// Обозначение: Стс
	SocialContribRate float32

	// Коэффициент, учитывающий особенности рынка труда в городах с численностью населения свыше миллиона человек
	// Источник: Минтранс России
	// Обозначение: Км
	LaborMarketCorrection float32

	// Отношение суммы прочих расходов по обычным видам деятельности и косвенных расходов к переменным расходам
	// Источник: Минтранс России
	// Обозначение: Кпр
	ExpensesRatio float32

	// Коэффициент, характеризующий продолжительность подготовительно-заключительного времени, времени прохождения
	// предрейсовых инструктажей и медицинских осмотров водителя
	// Источник: Минтранс России
	// Обозначение: Кпз
	Preparation float32

	// Годовой фонд рабочего времени водителя транспортных средств при соблюдении нормальной, 40 часовой,
	// продолжительности рабочего времени неделю
	// Источник: Минтранс России
	// Обозначение: ФРВв
	DriverWorkingTime float32

	// Годовой фонд рабочего времени кондуктора при соблюдении нормальной,
	// 40-часовой продолжительности рабочего времени в неделю
	// Источник: Минтранс России
	// Обозначение: ФРВк
	ConductorWorkingTime float32

	// Годовой фонд рабочего времени ремонтного рабочего при соблюдении нормальной,
	// 40 часовой, продолжительности рабочего времени в неделю
	// Источник: Минтранс России
	// Обозначение: ФРВрр
	RepairWorkingTime float32

	// Цена 1 литра топлива, указанная в последней, предшествующей дате размещения информации об НМЦК,
	// официальной публикации территориального органа Росстата, руб. (Цti)
	// Источник: Росстат
	// Обозначение: Сi (Цti)
	FuelLiterPrice float32

	// Базовые удельные расходы на шины i-го класса в расчете на 1 км пробега транспортных средств i-го класса
	// Источник: Минтранс России
	// Обозначение: Ушi
	TiresPrice float32

	// Базовая удельная трудоемкость технического обслуживания транспортных средств i-го класса, час./1000 км
	// Источник: Минтранс России
	// Обозначение: Тti
	MaintenanceComplexity float32

	// Базовая удельная трудоемкость текущего ремонта транспортных средств i-го класса
	// Источник: Минтранс России
	// Обозначение: Тpi
	RepairComplexity float32

	// Базовые удельные расходы на запасные части и материалы для транспортных
	// средств i-го класса в расчете на 1 км пробега
	// Источник: Минтранс России
	// Обозначение: Узчi км
	PartsExpenses float32

	// Индекс потребительских цен для t-ого года срока действия контракта, принимаемый в соответствии
	// с публикуемыми Минэкономразвития России прогнозами социально-экономического развития РФ
	// Источник: Минэконом России
	// Обозначение: Iпцtд
	ConsumerPriceIndex float32

	// Планируемый пробег транспортных средств i-го класса в t-ом году срока действия контракта
	// Источник: Минтранс России
	// Обозначение: Lti
	PlannedVehicleMileage float32

	// Планируемое количество часов работы транспортных средств i-го класса в t-ом году срока действия контракта
	// Источник: Минтранс России
	// Обозначение: АЧti
	PlannedVehicleTime float32

	// Нормативный расход топлива в расчете на 1 км пробега автобусов i-го класса, определенный в соответствии
	// с пунктом 40 Инструкции по учету доходов и расходов по обычным видам деятельности на автомобильном транспорте,
	// утвержденной приказом Минтранса России от 24 июня 2003 г. N 153
	// Источник: Минтранс России
	// Обозначение: Ri
	NormativeFuelRate float32

	// Индекс цен производителей нефтепродуктов для t-ого года срока действия контракта
	// Источник: Росстат
	// Обозначение: IТt
	OilProducersPriceIndex float32

	// Индекс цен на машины и оборудование для t-ого года срока действия контракта
	// Источник: Росстат
	// Обозначение: Iмt
	VehiclePriceIndex float32
}

func NewCoefficients() *Coefficients {
	return &Coefficients{
		AvgSalary:              31242,
		SocialContribRate:      0.3,
		LaborMarketCorrection:  0.8,
		ExpensesRatio:          0.6,
		Preparation:            1.05,
		DriverWorkingTime:      1744,
		ConductorWorkingTime:   1792,
		RepairWorkingTime:      1832,
		FuelLiterPrice:         41.4,
		TiresPrice:             0.66,
		MaintenanceComplexity:  9.3,
		RepairComplexity:       7.8,
		PartsExpenses:          3.6,
		ConsumerPriceIndex:     1.05,
		PlannedVehicleMileage:  1000,
		PlannedVehicleTime:     17,
		NormativeFuelRate:      0.35,
		OilProducersPriceIndex: 1.17,
		VehiclePriceIndex:      0.98,
	}
}

// Коэффициенты, учитывающие дифференциацию заработных плат работников в зависимости
// от класса транспортных средств и вида маршрутов (Кзпi)
// http://www.consultant.ru/document/cons_doc_LAW_287437/35f57a75bb1aa5d262264c65395c23bb5987772f/#dst100106
// [!!!] Только для "Прочие маршруты"
func getSalaryCoef(workerCategory int, date time.Time) float32 {
	salaryCoefs := make(map[int]float32)

	// Коэффициенты до 01.01.2022
	if date.Before(time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)) {
		salaryCoefs[WORKER_BUS_DRIVER_EXTRA_SMALL] = 0.95
		salaryCoefs[WORKER_BUS_DRIVER_SMALL] = 1.0
		salaryCoefs[WORKER_BUS_DRIVER_STANDART] = 1.1
		salaryCoefs[WORKER_BUS_DRIVER_BIG] = 1.6
		salaryCoefs[WORKER_BUS_DRIVER_EXTRA_BIG] = 1.7
		salaryCoefs[WORKER_BUS_CONDUCTOR] = 0.8
		salaryCoefs[WORKER_BUS_REPAIRMAN] = 0.9
	} else {
		salaryCoefs[WORKER_BUS_DRIVER_EXTRA_SMALL] = 1.08
		salaryCoefs[WORKER_BUS_DRIVER_SMALL] = 1.15
		salaryCoefs[WORKER_BUS_DRIVER_STANDART] = 1.28
		salaryCoefs[WORKER_BUS_DRIVER_BIG] = 1.85
		salaryCoefs[WORKER_BUS_DRIVER_EXTRA_BIG] = 1.95
		salaryCoefs[WORKER_BUS_CONDUCTOR] = 0.95
		salaryCoefs[WORKER_BUS_REPAIRMAN] = 1.0
	}

	return salaryCoefs[workerCategory]
}

func getSalaryCorrectionMaintenance(region int) float32 {
	corrections := make(map[int]float32)
	corrections[SALARY_CORRECTION_REGION_CLASS_1] = 1.0
	corrections[SALARY_CORRECTION_REGION_CLASS_2] = 0.9
	corrections[SALARY_CORRECTION_REGION_CLASS_3] = 0.9
	corrections[SALARY_CORRECTION_REGION_CLASS_4] = 0.8
	corrections[SALARY_CORRECTION_REGION_OTHER] = 1.0

	return corrections[region]
}

func getSalaryCorrectionRepair(region int) float32 {
	corrections := make(map[int]float32)
	corrections[SALARY_CORRECTION_REGION_CLASS_1] = 0.9
	corrections[SALARY_CORRECTION_REGION_CLASS_2] = 1.1
	corrections[SALARY_CORRECTION_REGION_CLASS_3] = 1.2
	corrections[SALARY_CORRECTION_REGION_CLASS_4] = 1.3
	corrections[SALARY_CORRECTION_REGION_OTHER] = 1.0

	return corrections[region]
}

func getSalaryCorrectionSpareParts(region int) float32 {
	corrections := make(map[int]float32)
	corrections[SALARY_CORRECTION_REGION_CLASS_1] = 0.9
	corrections[SALARY_CORRECTION_REGION_CLASS_2] = 1.1
	corrections[SALARY_CORRECTION_REGION_CLASS_3] = 1.25
	corrections[SALARY_CORRECTION_REGION_CLASS_4] = 1.4
	corrections[SALARY_CORRECTION_REGION_OTHER] = 1.0

	return corrections[region]
}
