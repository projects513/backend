package core

type Core struct {
	Coef *Coefficients
}

func NewCalculator(coefficients *Coefficients) *Core {
	return &Core{coefficients}
}

func GetTransportTypeList() map[int]string {
	result := make(map[int]string)
	result[WORKER_BUS_DRIVER_EXTRA_SMALL] = "Автобус особо малого класса"
	result[WORKER_BUS_DRIVER_SMALL] = "Автобус малого класса"
	result[WORKER_BUS_DRIVER_STANDART] = "Автобус среднего класса"
	result[WORKER_BUS_DRIVER_BIG] = "Автобус большого класса"
	result[WORKER_BUS_DRIVER_EXTRA_BIG] = "Автобус особо большого класса"

	return result
}
