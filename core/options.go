package core

type Options struct {
	// Тип транспорта
	TransportType int

	// Унифицированный код региона РФ
	RegionId string

	// Транспортная норма расхода топлива на пробег автобуса  i-го класса в расчете на 100 км
	// Обозначение: Hsi
	FuelRate float32

	// Поправочный коэффициент к норме, учитывающий особенности эксплуатации
	// Обозначение: D
	FuelRateCorrection float32

	// Норма расхода топлива на работу отопителей салона
	// Обозначение: HoTi
	HeatingFuelRate float32

	// Принимаемое в соответствиями с условиями контракта количество месяцев работы отопителя
	// Обозначение: Nз
	HeatingMonthCount int

	// Планируемая в соответствии с расписанием эксплутационная скорость
	// Обозначение: Vэ
	OperationSpeed float32
}

func (options *Options) getRegionGroup() int {
	regions := NewRegionCollection()
	region, err := regions.FindRegionById(options.RegionId)
	if err != nil {
		return regions.GetDefaultGroupId()
	}

	return region.GroupId
}
