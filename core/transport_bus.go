package core

import "time"

// РАСЧЕТ МАКСИМАЛЬНОЙ СЕБЕСТОИМОСТИ 1 КМ ПРОБЕГА АВТОБУСОВ
// Sti = РОТВti + РОТКti + CPti + Ртti + Рсмti + Ршti + РТОti + ПКРti, руб./км
func (core *Core) CalcBusCostPrice(options *Options) float32 {
	return core.calcDriverSalary(options) + core.calcConductorSalary() + core.calcSocialPayments(options) +
		core.calcFuelCosts(options) + core.calcOtherMaterialsCosts(options) + core.calcTiresCosts() +
		core.calcMaintenanceCosts(options) + core.calcOtherCosts(options)
}

// Расходы на оплату труда водителей транспортных средств i-го класса в t-ый год срока
// действия контракта в расчете на 1 км пробега (РОТi)
// РОТВti = 12 * 1.2 * ЗПВmaxi * АЧti * kпз * Iпцt / (Lti * ФРВв), руб./км
func (core *Core) calcDriverSalary(options *Options) float32 {
	// Средняя месячная оплата труда водителя транспортных средств i-го класса
	// ЗПВi = СЗП x Кзпi x Км, руб.
	salary := core.Coef.AvgSalary * getSalaryCoef(options.TransportType, time.Now()) *
		core.Coef.LaborMarketCorrection

	return 12 * 1.2 * salary * core.Coef.PlannedVehicleTime *
		core.Coef.Preparation * core.Coef.ConsumerPriceIndex /
		(core.Coef.PlannedVehicleMileage * core.Coef.DriverWorkingTime)
}

// Расходы на оплату труда кондукторов транспортных средств i-го класса в t-ом году срока
// действия контракта в расчете на 1 км пробега
// РОТКti = 12 * 1.2 * ЗПКmaxi * АЧКti * 1.05  * Iпцt / (Lti * ФРВк), руб./км
func (core *Core) calcConductorSalary() float32 {
	// Средняя месячная оплата труда кондуктора транспортных средств i-го класса
	// ЗПКi = СЗП x Кзпi x Км, руб.
	salary := core.Coef.AvgSalary * getSalaryCoef(WORKER_BUS_CONDUCTOR, time.Now()) * core.Coef.LaborMarketCorrection

	return 12 * 1.2 * salary * core.Coef.PlannedVehicleTime * 1.05 * core.Coef.ConsumerPriceIndex /
		(core.Coef.PlannedVehicleMileage * core.Coef.ConductorWorkingTime)
}

// Расходы на оплату труда ремонтных рабочих с отчислениями на социальные нужды в расчете на 1 км пробега автобусов
// i-го класса в t-ый год срока действия контракта (ФОТррi)
// ФОТррi = 0.001 * 12 * 1.2 * Iпцt * ЗПР * (Tti / Kзn + Tpi * Kз) / ФРВрр * (1 + Cтфс / 100)
func (core *Core) calcRepairmanSalary(options *Options) float32 {
	// Расчетная часовая оплата труда ремонтного рабочего (ЗПР)
	// ЗПР = СЗП x Кзпi x Км, руб.
	salary := core.Coef.AvgSalary * getSalaryCoef(WORKER_BUS_REPAIRMAN, time.Now()) * core.Coef.LaborMarketCorrection

	return 0.001 * 12 * 1.2 * core.Coef.ConsumerPriceIndex * salary *
		((core.Coef.MaintenanceComplexity / getSalaryCorrectionMaintenance(options.getRegionGroup())) +
			(core.Coef.RepairComplexity * getSalaryCorrectionRepair(options.getRegionGroup()))) /
		core.Coef.RepairWorkingTime * (1 + core.Coef.SocialContribRate/100)
}

// Отчисления на социальные нужды от оплаты труда водителей и кондукторов транспортных средств i-го класса
// в t-ый год срока действия контракта в расчете на 1 км пробега (CPti)
// CPti = (РОТВti + РОТКti) x (Стс / 100), руб./км
func (core *Core) calcSocialPayments(options *Options) float32 {
	return (core.calcDriverSalary(options) + core.calcConductorSalary()) * (core.Coef.SocialContribRate / 100)
}

// Расходы на топливо для транспорта i-го класса в t-ый год срока действия контракта расчете на 1 км пробега (Ртti)
// Ртti = Ci x Iтt x Ri, руб.
func (core *Core) calcFuelCosts(options *Options) float32 {
	//return core.Coef.FuelLiterPrice * core.Coef.NormativeFuelRate * core.Coef.OilProducersPriceIndex
	Ri := options.FuelRate*(1+0.01*options.FuelRateCorrection) +
		(options.HeatingFuelRate / options.OperationSpeed * float32(options.HeatingMonthCount) / 12)

	return (core.Coef.FuelLiterPrice * core.Coef.OilProducersPriceIndex * Ri) / 100

}

// Расходы на запасные части и материалы, используемые при техническом обслуживании и ремонте транспортных средств
// i-го класса в t-ый год срока действия контракта в расчете на 1 км пробега (РЗЧti)
// РЗЧti = Узч i км x Кзч x Iмt
func (core *Core) calcSparePartsCosts(options *Options) float32 {
	return core.Coef.PartsExpenses * getSalaryCorrectionSpareParts(options.getRegionGroup()) *
		core.Coef.VehiclePriceIndex
}

// Расходы на смазочные и прочие эксплуатационные материалы для транспортных средств i-го класса в t-ый год
// срока действия контракта в расчете на 1 км пробега (Рсмti)
// Рсмti = 0,075 x Ртti, руб./км
func (core *Core) calcOtherMaterialsCosts(options *Options) float32 {
	return 0.075 * core.calcFuelCosts(options)
}

// Расходы на износ и ремонт шин транспортных средств i-го класса в t-ый год
// срока действия контракта в расчете на 1 км пробега (Ршti)
// Ршti = Ушi x Iмt, руб./км
func (core *Core) calcTiresCosts() float32 {
	return core.Coef.TiresPrice * core.Coef.VehiclePriceIndex
}

// Расходы на техническое обслуживание и ремонт транспортных средств i-го класса в t-ый год
// срока действия контракта в расчете на 1 км пробега (РТОti)
// РТОti = ФОТррi + РЗЧti, руб./км
func (core *Core) calcMaintenanceCosts(options *Options) float32 {
	return core.calcRepairmanSalary(options) + core.calcSparePartsCosts(options)
}

// Прочие расходы по обычным видам деятельности в сумме с косвенными расходами для транспортных средств
// i-го класса в t-ом году срока действия контракта (ПКРti)
// ПКРti = Kпр x (Ртti + Рсмti + Ршti + РТОti), руб./км
func (core *Core) calcOtherCosts(options *Options) float32 {
	return core.Coef.ExpensesRatio *
		(core.calcFuelCosts(options) + core.calcOtherMaterialsCosts(options) +
			core.calcTiresCosts() + core.calcMaintenanceCosts(options))
}
