package handlers

import (
	"fmt"
	"github.com/jinzhu/copier"
	"github.com/labstack/echo/v4"
	"net/http"
	"project513/core"
)

type CoefficientsResponse struct {
	AvgSalary              float32 `json:"avg_salary"`
	SocialContribRate      float32 `json:"social_contrib_rate"`
	LaborMarketCorrection  float32 `json:"labor_market_correction"`
	ExpensesRatio          float32 `json:"expenses_ratio"`
	Preparation            float32 `json:"preparation_coef"`
	DriverWorkingTime      float32 `json:"driver_working_time"`
	ConductorWorkingTime   float32 `json:"conductor_working_time"`
	RepairWorkingTime      float32 `json:"repair_working_time"`
	FuelLiterPrice         float32 `json:"fuel_liter_price"`
	TiresPrice             float32 `json:"tires_price"`
	MaintenanceComplexity  float32 `json:"maintenance_complexity"`
	RepairComplexity       float32 `json:"repair_complexity"`
	PartsExpenses          float32 `json:"parts_expenses"`
	ConsumerPriceIndex     float32 `json:"consumer_price_index"`
	PlannedVehicleMileage  float32 `json:"planned_vehicle_mileage"`
	PlannedVehicleTime     float32 `json:"planned_vehicle_time"`
	NormativeFuelRate      float32 `json:"normative_fuel_rate"`
	OilProducersPriceIndex float32 `json:"oil_producers_price_index"`
	VehiclePriceIndex      float32 `json:"vehicle_price_index"`
}

func CoefficientsHandler(c echo.Context) error {
	response := &CoefficientsResponse{}
	if err := copier.Copy(response, core.NewCoefficients()); err != nil {
		fmt.Println(err)
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, response)
}
