package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"project513/core"
)

func RegionsHandler(c echo.Context) error {
	regions := core.NewRegionCollection()
	response := make(core.RegionCollection, 0, len(*regions))

	for _, region := range *regions {
		response = append(response, region)
	}

	return c.JSON(http.StatusOK, response)
}
