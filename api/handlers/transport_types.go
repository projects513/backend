package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"project513/core"
)

type transportType struct {
	Id    int    `json:"id"`
	Title string `json:"title"`
}

func TransportTypeHandler(c echo.Context) error {
	types := core.GetTransportTypeList()
	response := make([]*transportType, 0, len(types))
	for id, title := range types {
		response = append(response, &transportType{id, title})
	}

	return c.JSON(http.StatusOK, response)
}
