package handlers

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/jinzhu/copier"
	"github.com/labstack/echo/v4"
	"net/http"
	"project513/core"
)

type calculateRequest struct {
	TransportType      int                  `json:"transport_type"`
	RegionId           string               `json:"region_id"`
	FuelRate           float32              `json:"fuel_norm"`
	FuelRateCorrection float32              `json:"correction_coef"`
	HeatingFuelRate    float32              `json:"heating_norm"`
	HeatingMonthCount  int                  `json:"months"`
	OperationSpeed     float32              `json:"velocity"`
	Extra              CoefficientsResponse `json:"extra"`
}

type calculateResponse struct {
	Result float32 `json:"result"`
}

func CalculateHandler(c echo.Context) error {
	request := new(calculateRequest)
	if err := c.Bind(request); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if err := request.Validate(); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	options := &core.Options{}
	if err := copier.Copy(options, request); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	coefficients := core.NewCoefficients()
	if err := copier.Copy(coefficients, request.Extra); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	calculator := core.NewCalculator(coefficients)
	result := &calculateResponse{
		calculator.CalcBusCostPrice(options),
	}

	return c.JSON(http.StatusOK, result)
}

func (data calculateRequest) Validate() error {
	return validation.ValidateStruct(&data,
		validation.Field(&data.TransportType, validation.Required),
		validation.Field(&data.RegionId, validation.Required),
		validation.Field(&data.FuelRate, validation.Required),
		validation.Field(&data.FuelRateCorrection, validation.Required),
		validation.Field(&data.HeatingFuelRate, validation.Required),
		validation.Field(&data.HeatingMonthCount, validation.Required),
		validation.Field(&data.OperationSpeed, validation.Required),
	)
}
