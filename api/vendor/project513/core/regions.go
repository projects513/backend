package core

import "errors"

const (
	SALARY_CORRECTION_REGION_CLASS_1 = iota + 1
	SALARY_CORRECTION_REGION_CLASS_2
	SALARY_CORRECTION_REGION_CLASS_3
	SALARY_CORRECTION_REGION_CLASS_4
	SALARY_CORRECTION_REGION_OTHER
)

type RegionCollection []*Region

type Region struct {
	Id      string `json:"id"`
	Title   string `json:"title"`
	GroupId int    `json:"group_id"`
}

func NewRegionCollection() *RegionCollection {
	return &RegionCollection{
		{"01", "Республика Адыгея (Адыгея)", SALARY_CORRECTION_REGION_OTHER},
		{"04", "Республика Алтай", SALARY_CORRECTION_REGION_CLASS_3},
		{"02", "Республика Башкортостан", SALARY_CORRECTION_REGION_CLASS_2},
		{"03", "Республика Бурятия", SALARY_CORRECTION_REGION_CLASS_3},
		{"05", "Республика Дагестан", SALARY_CORRECTION_REGION_CLASS_1},
		{"06", "Республика Ингушетия", SALARY_CORRECTION_REGION_CLASS_1},
		{"07", "Кабардино-Балкарская Республика", SALARY_CORRECTION_REGION_CLASS_1},
		{"08", "Республика Калмыкия", SALARY_CORRECTION_REGION_OTHER},
		{"09", "Карачаево-Черкесская Республика", SALARY_CORRECTION_REGION_OTHER},
		{"10", "Республика Карелия", SALARY_CORRECTION_REGION_CLASS_3},
		{"11", "Республика Коми", SALARY_CORRECTION_REGION_CLASS_3},
		{"91", "Республика Крым", SALARY_CORRECTION_REGION_CLASS_1},
		{"12", "Республика Марий Эл", SALARY_CORRECTION_REGION_OTHER},
		{"13", "Республика Мордовия", SALARY_CORRECTION_REGION_OTHER},
		{"14", "Республика Саха (Якутия)", SALARY_CORRECTION_REGION_CLASS_4},
		{"15", "Республика Северная Осетия – Алания", SALARY_CORRECTION_REGION_CLASS_1},
		{"16", "Республика Татарстан", SALARY_CORRECTION_REGION_OTHER},
		{"17", "Республика Тыва", SALARY_CORRECTION_REGION_CLASS_3},
		{"18", "Удмуртская Республика", SALARY_CORRECTION_REGION_CLASS_2},
		{"19", "Республика Хакасия", SALARY_CORRECTION_REGION_CLASS_3},
		{"20", "Чеченская Республика", SALARY_CORRECTION_REGION_CLASS_1},
		{"21", "Чувашская Республика – Чувашия", SALARY_CORRECTION_REGION_OTHER},
		{"22", "Алтайский край", SALARY_CORRECTION_REGION_CLASS_3},
		{"75", "Забайкальский край", SALARY_CORRECTION_REGION_CLASS_3},
		{"41", "Камчатский край", SALARY_CORRECTION_REGION_CLASS_3},
		{"23", "Краснодарский край", SALARY_CORRECTION_REGION_CLASS_1},
		{"24", "Красноярский край", SALARY_CORRECTION_REGION_CLASS_3},
		{"59", "Пермский край", SALARY_CORRECTION_REGION_CLASS_2},
		{"25", "Приморский край", SALARY_CORRECTION_REGION_CLASS_3},
		{"26", "Ставропольский край", SALARY_CORRECTION_REGION_CLASS_1},
		{"27", "Хабаровский край", SALARY_CORRECTION_REGION_CLASS_3},
		{"28", "Амурская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"29", "Архангельская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"30", "Астраханская область", SALARY_CORRECTION_REGION_OTHER},
		{"31", "Белгородская область", SALARY_CORRECTION_REGION_OTHER},
		{"32", "Брянская область", SALARY_CORRECTION_REGION_OTHER},
		{"33", "Владимирская область", SALARY_CORRECTION_REGION_OTHER},
		{"34", "Волгоградская область", SALARY_CORRECTION_REGION_OTHER},
		{"35", "Вологодская область", SALARY_CORRECTION_REGION_OTHER},
		{"36", "Воронежская область", SALARY_CORRECTION_REGION_OTHER},
		{"37", "Ивановская область", SALARY_CORRECTION_REGION_CLASS_1},
		{"38", "Иркутская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"39", "Калининградская область", SALARY_CORRECTION_REGION_CLASS_1},
		{"40", "Калужская область", SALARY_CORRECTION_REGION_OTHER},
		{"42", "Кемеровская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"43", "Кировская область", SALARY_CORRECTION_REGION_OTHER},
		{"44", "Костромская область", SALARY_CORRECTION_REGION_OTHER},
		{"45", "Курганская область", SALARY_CORRECTION_REGION_CLASS_2},
		{"46", "Курская область", SALARY_CORRECTION_REGION_OTHER},
		{"47", "Ленинградская область", SALARY_CORRECTION_REGION_OTHER},
		{"48", "Липецкая область", SALARY_CORRECTION_REGION_OTHER},
		{"49", "Магаданская область", SALARY_CORRECTION_REGION_CLASS_4},
		{"50", "Московская область", SALARY_CORRECTION_REGION_OTHER},
		{"51", "Мурманская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"52", "Нижегородская область", SALARY_CORRECTION_REGION_OTHER},
		{"53", "Новгородская область", SALARY_CORRECTION_REGION_OTHER},
		{"54", "Новосибирская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"55", "Омская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"56", "Оренбургская область", SALARY_CORRECTION_REGION_OTHER},
		{"57", "Орловская область", SALARY_CORRECTION_REGION_OTHER},
		{"58", "Пензенская область", SALARY_CORRECTION_REGION_OTHER},
		{"60", "Псковская область", SALARY_CORRECTION_REGION_OTHER},
		{"61", "Ростовская область", SALARY_CORRECTION_REGION_CLASS_1},
		{"62", "Рязанская область", SALARY_CORRECTION_REGION_OTHER},
		{"63", "Самарская область", SALARY_CORRECTION_REGION_OTHER},
		{"64", "Саратовская область", SALARY_CORRECTION_REGION_OTHER},
		{"65", "Сахалинская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"66", "Свердловская область", SALARY_CORRECTION_REGION_CLASS_2},
		{"67", "Смоленская область", SALARY_CORRECTION_REGION_OTHER},
		{"68", "Тамбовская область", SALARY_CORRECTION_REGION_OTHER},
		{"69", "Тверская область", SALARY_CORRECTION_REGION_OTHER},
		{"70", "Томская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"71", "Тульская область", SALARY_CORRECTION_REGION_OTHER},
		{"72", "Тюменская область", SALARY_CORRECTION_REGION_CLASS_3},
		{"73", "Ульяновская область", SALARY_CORRECTION_REGION_OTHER},
		{"74", "Челябинская область", SALARY_CORRECTION_REGION_CLASS_2},
		{"76", "Ярославская область", SALARY_CORRECTION_REGION_OTHER},
		{"77", "Город Москва", SALARY_CORRECTION_REGION_OTHER},
		{"78", "Город Санкт-Петербург", SALARY_CORRECTION_REGION_OTHER},
		{"92", "Город Севастополь", SALARY_CORRECTION_REGION_CLASS_1},
		{"79", "Еврейская автономная область", SALARY_CORRECTION_REGION_OTHER},
		{"83", "Ненецкий автономный округ", SALARY_CORRECTION_REGION_CLASS_3},
		{"86", "Ханты-Мансийский автономный округ", SALARY_CORRECTION_REGION_CLASS_3},
		{"87", "Чукотский автономный округ", SALARY_CORRECTION_REGION_CLASS_4},
		{"89", "Ямало-Ненецкий автономный округ", SALARY_CORRECTION_REGION_CLASS_3},
	}
}

func (regions *RegionCollection) FindRegionById(id string) (*Region, error) {
	for _, region := range *regions {
		if region.Id == id {
			return region, nil
		}
	}

	return nil, errors.New("region not found")
}

func (regions *RegionCollection) GetDefaultGroupId() int {
	return SALARY_CORRECTION_REGION_OTHER
}
