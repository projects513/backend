package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"project513/api/handlers"
)

type Host struct {
	Echo *echo.Echo
}

func main() {
	hosts := map[string]*Host{}

	// API
	api := echo.New()
	api.Use(middleware.Logger())
	api.Use(middleware.Recover())
	api.Use(middleware.CORS())
	hosts["api.project513.loc"] = &Host{api}

	api.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, version)
	})
	api.GET("/regions", handlers.RegionsHandler)
	api.GET("/transport_types", handlers.TransportTypeHandler)
	api.GET("/coefficients", handlers.CoefficientsHandler)
	api.POST("/calculate", handlers.CalculateHandler)

	// Frontend (static only)
	frontend := echo.New()
	frontend.Use(middleware.Logger())
	frontend.Use(middleware.Recover())
	frontend.Use(middleware.CORS())
	frontend.Static("/", "public")
	hosts["project513.loc"] = &Host{frontend}

	// Core server
	e := echo.New()
	e.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		host := hosts[req.Host]

		if host == nil {
			err = echo.ErrNotFound
		} else {
			host.Echo.ServeHTTP(res, req)
		}
		return
	})
	e.Logger.Fatal(e.Start(":80"))
}
